# Docker CI images with /eos

This repository contains unofficial base docker images with the standard `/eos`
mountpoints. Please note they can only be used within privileged containers.

## How to use interactively from the command-line

```
$ sudo docker run --rm --net=host --privileged -it --entrypoint /bin/bash gitlab-registry.cern.ch/eos/gitlab-eos/cc7:latest

# automount
# kinit <username>
# eosfusebind
# ls -l /eos
dr-xr-xr-x 2 root root 0 Mar  9 16:19 ams
dr-xr-xr-x 2 root root 0 Mar  9 16:19 atlas
dr-xr-xr-x 2 root root 0 Mar  9 16:19 cms
dr-xr-xr-x 2 root root 0 Mar  9 16:19 experiment
dr-xr-xr-x 2 root root 0 Mar  9 16:19 home-a
dr-xr-xr-x 2 root root 0 Mar  9 16:19 home-b
....
```

## How to use from Gitlab CI

* Set up a Gitlab CI secret variable, such as `$USERPASS` in `Settings -> CI / CD / -> Variables`. Make
sure it is set to **protected**, otherwise any random person making a pull request could trivially gain access to it.

```
rpms:
  stage: publish
  image: gitlab-registry.cern.ch/eos/gitlab-eos/cc7:latest
  script:
   - automount
   - echo "$USERPASS" | kinit username
   - eosfusebind
   - ls -l /eos/...
  tags:
   - docker-privileged
```